# MIPS assembly language programming

This directory contains the samples of mips assembly language
programming. You should be able to run this samples with the [SPIM]
simulator.


## Getting started.

Debian based systems already comes with a [`spim`][spim-package]. You
can install it using the command:

```
sudo apt install spim

```

The [SPIM manual][spim-manual] is available online. You can start
using it. See the [hello.mips](./hello.mips) file for an example [MIPS] assembly
language program suitable to be executed by [SPIM].


[SPIM]: <http://spimsimulator.sourceforge.net/> "SPIM: A MIPS simulator"
[MIPS]: <https://en.wikipedia.org/wiki/MIPS_architecture> "MIPS architecture"
[spim-manual]: <http://www.cs.utexas.edu/users/mckinley/352/xspim/spim.pdf> "SPIM Manual"
[spim-package]: <https://packages.debian.org/source/sid/spim> "SPIM on Debian"
