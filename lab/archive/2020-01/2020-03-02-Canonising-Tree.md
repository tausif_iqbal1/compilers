# Canonising Tree language.

**NOTE:**This weeks assignment has only the project component and
hence all code that you write as part of this assignment should go to
your project subdirectory of your repository.

In the last assignment we converted tiger code into the intermediate
language called tree. We further refine the tree language by
canonising it. This means the following.

1. The entire program becomes a simple list of statements and the
   statements or expressions do not themselves contain the `SEQ` or
   `ESEQ` constructors.
   
2. Function calls move to the top level. i.e. its parent is a `EXP` or
   a `MOVE (t, _)`

Having done this the program just becomes a list of statements. We
break the program into basic blocks and find traces. See the Chapter 8
of the book.
