# Course on Compiler Design.

[![Build Status][build-status-img]][build-status]

This is the repository for the first course on compiler construction
that I teach at IIT Palakkad. The material available here are relevant
to the course CS3300 titled "Compiler Design" and its associated
laboratory CS3310 titled "Compiler Design Laboratory". Here you will
find notes, code samples, and other information that are relevant for
this course. Students are encouraged to use the wiki that is available
with this repository for information. We also encourage responsible
editing of the wiki.

Some important links

* [Course Repository]

* [Course Wiki]

* [Issue tracker]

* [Lab problems](lab)

For any problems regarding the course please use the issue
tracker. Remember that the issue tracker is public and any one can see
the discussions therein. This has privacy implications and you might
not want to discuss things like your grade.

## Testing your setup

We have included some tests in the source files. You can run this
using the the make command. This will ensure that you have setup your
machine correctly with the correct software for this course.to this.


```
make test   # run the tests on the sample programs here.
make clean  # cleanup the directories of temporary files.

```

## Continuous Build system.

We now use the [gitlab pipelines][ci-pipelines] for continuous build.

## Other resources.

If you are familiar with Docker you can use the docker instance built
using the [`Dockerfile`][dockerfile] in this repository to try out the
code samples in this repository. Images are available at dockerhub
under the url <https://hub.docker.com/r/piyushkurur/compilers/>


[Course Repository]: <https://gitlab.com/piyush-kurur/compilers>
[Course Wiki]:       <https://gitlab.com/piyush-kurur/compilers/wikis/Home>
[Issue tracker]:     <https://gitlab.com/piyush-kurur/compilers/issues>
[dockerfile]: <Dockerfile>
[build-status-img]: <https://gitlab.com/piyush-kurur/compilers/badges/master/pipeline.svg> "Build Status"
[build-status]: <https://gitlab.com/piyush-kurur/compilers/pipelines/latest> "Build Status"
[build-status]: <https://gitlab.com/piyush-kurur/compilers/pipelines/> "CI pipelines"
